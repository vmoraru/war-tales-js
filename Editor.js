this._wt = this._wt || {};

(function() {

	this._wt.Editor = function(stage, mapSettings) {

		_wt.stage = stage;
		_wt.preloader = new _wt.GamePreloader();
		$(_wt.evtDispatcher).on(_wt.CoreEvents.PRELOAD_COMPLETE, function(e) {
			console.log("[Event] " + e.type);
			_wt.gameBoard = new _wt.GameBoard(mapSettings);

			var ui = new _wt.EditorGui();
		});
	}

	/**
	* EditorGui
	**/
	this._wt.EditorGui = function() {
		var c = $(_wt.preloader.getResult("editor_ui"));
		$("#UI").append(c);
		var tilePanelEl = c.find(".tile_panel");
		
		initTexturePanel();

		function initTexturePanel() {
			var terrainJson = _wt.globals.terrain;
			for (var type in terrainJson) {
				// append title
				tilePanelEl.append(buildSectionTitle(type));

				var textureSet = terrainJson[type];
				if(_.isArray(textureSet)) {
					// multiple textures for this section
					for (var texture in textureSet) {
						tilePanelEl.append(buildTextureTile(textureSet[texture].name));
					}
				} else {
					// a single texture for this section
					tilePanelEl.append(buildTextureTile(textureSet.name));
				}
			}
		}

		function buildSectionTitle(title) {
			return $('<div></div>').prop("class", "section").text(title);
		}

		function buildTextureTile(textureName) {
			var tileEl = $('<div></div>') .prop("class", "tile");
			var img = buildImgDomElement(textureName);
			$(tileEl).append(img);

			return tileEl;
		}

		function buildImgDomElement(name) {
			// get the img element from the preloader, associated with this texture name
			// clone it, then set the preferred size
			return $(_wt.preloader.getFeatureTexture(name)).clone().width("90%").height("90%");
		}
		
	}

}());