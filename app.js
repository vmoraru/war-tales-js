$(document).ready(function(){

	var stage = new createjs.Stage("gameCanvas");
	
	function runMapScroll() {
		var game = new MapScrollTest(stage);
		game.preloadGame();
	}
	
	function runCombatSim() {
		var sim = new CombatSim();
		sim.init();
		sim.start();
	}

	function runGame() {
		var rows = 5;
		var cols = 10;
		var tiles = [
			[{terrain:_wt.TerrainEnum.PLAINS}, {terrain:_wt.TerrainEnum.HILLS}, {terrain:_wt.TerrainEnum.HILLS}],
			[{terrain:_wt.TerrainEnum.URBAN}, {terrain:_wt.TerrainEnum.PLAINS}, {terrain:_wt.TerrainEnum.MOUNTAIN}]
		];
		var mapSettings = {"rows": rows, "cols": cols, "tiles": tiles };
		var config = {
			players: ["P1", "P2"]
		};

		var game = new _wt.Game(stage, mapSettings, config);
		// var editor = new _wt.GameEditor(stage);
	}

	function runEditor() {
		var rows = 8;
		var cols = 12;
		var mapSettings = {"rows": rows, "cols": cols};
		var editor = new _wt.Editor(stage, mapSettings);
	}
	
	// resize handing code
	resize();
	window.addEventListener('resize', resize, false);
	function resize() {
		console.log("resize");
		stage.canvas.width = window.innerWidth;
		stage.canvas.height = window.innerHeight;
	}

	// update each frame
	createjs.Ticker.addEventListener("tick", stage);

	// runMapScroll();
	// runGame();
	runEditor();
})