this._wt = this._wt || {};

(function() {

	this._wt.GamePreloader = function() {
		var dataQueue = new createjs.LoadQueue();
		var textureQueue = new createjs.LoadQueue();

		dataQueue.on("complete", handleDataComplete);
		dataQueue.loadManifest([
			{ id: "terrain", src: "assets/terrain.json", callback: "terrain"},
			{ id: "game_settings", src: "assets/game_settings.json", callback: "gameSettings"},
			// TODO only for the editor
			{ id: "editor_ui", src:"assets/editor.html"}
		]);

		function handleDataComplete(e) {
			console.log(e);

			// var data = {
			//     images: ["sprites.jpg"],
			//     frames: {width:50, height:50},
			//     animations: {
			//         stand:0,
			//         run:[1,5],
			//         jump:[6,8,"run"]
			//     }
			// };

			// var data = queue.getResult("sprites");
			// var spriteSheet = new createjs.SpriteSheet(data);

			_wt.globals.game_settings = dataQueue.getResult("game_settings");
			_wt.globals.terrain = dataQueue.getResult("terrain");

			var tileTextureManifest = [];
			var tileSetJson = dataQueue.getResult("terrain");
			for (attr in tileSetJson) {
				if (_.isArray(tileSetJson[attr])) {
					for (texture in tileSetJson[attr]) {
						addToTileTextureManifest(tileTextureManifest, tileSetJson[attr][texture].name);
					}
				} else {
					addToTileTextureManifest(tileTextureManifest, tileSetJson[attr].name);
				}
			}
			dataQueue.removeAllEventListeners();

			textureQueue = new createjs.LoadQueue();
			textureQueue.on("complete", handleTexturesComplete);
			textureQueue.loadManifest(tileTextureManifest);
		}

		function handleTexturesComplete(e) {
			console.log("Texture loading complete!");
			_wt.evtDispatcher.triggerHandler(_wt.CoreEvents.PRELOAD_COMPLETE);
		}

		function addToTileTextureManifest(manifest, textureName) {
			manifest.push({
				id : textureName,
			 	src : "assets/" + textureName  + ".png"
			});
		}

		/*function handleSoundsComplete(e) {
			queue.removeEventListener("complete");
		}*/

		this.getBaseTexture = function() {
			return textureQueue.getResult("base");
		}

		this.getFeatureTexture = function(textureName) {
			return textureQueue.getResult(textureName);
		}

		this.getResult = function(name){
			return dataQueue.getResult(name);
		}

	}
}());