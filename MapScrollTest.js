function MapScrollTest(_stage) {

	var self = this;
	var GameStateEnum = { SIMPLE:"Hex", NEIGHBORS:"Neighbors", DIAGONALS:"Diagonals" }

	this.gameState = GameStateEnum.SIMPLE;
	this.queue = new createjs.LoadQueue();
	this.stage = _stage;
	this.mapContainer = new createjs.Container();
	this.mapEventContainer = new createjs.Shape();
	this.hexuMap;
	this.tileMat;

	this.changeState = function() {
		switch(self.gameState) {
			case GameStateEnum.SIMPLE:
			self.gameState = GameStateEnum.NEIGHBORS;
			break;
			case GameStateEnum.NEIGHBORS:
			self.gameState = GameStateEnum.DIAGONALS;
			break;
			case GameStateEnum.DIAGONALS:
			self.gameState = GameStateEnum.SIMPLE;
			break;
		}
	}

	this.preloadGame = function() {
		self.queue.on("complete", handleComplete, self);
		self.queue.loadManifest([
			{id: "tile", src:"assets/base.png"},
			{id: "water", src:"assets/water_1.png"},
			{id: "hills1", src:"assets/hills_1.png"},
			{id: "hills2", src:"assets/hills_2.png"},
			{id: "plains1", src:"assets/plains_1.png"},
			{id: "plains2", src:"assets/plains_2.png"}
			]);
		function handleComplete() {
			console.log("Preload complete");
			self.mainMenu();
		}
	}

	this.mainMenu = function()	{
		var menu = new MainMenu(self);
		menu.show();
	}

	this.startGame = function() {
		self.stage.addChild(self.mapEventContainer);
		self.stage.addChild(self.mapContainer);
			// build h.u.d
			var hud = new Hud(self);
			hud.init();
			
			var tileAsset = self.queue.getResult("tile");
			var hillsAsset = self.queue.getResult("hills1");
			var hillsAsset2 = self.queue.getResult("hills2");
			var plainsAsset = self.queue.getResult("plains1");
			var plainsAsset2 = self.queue.getResult("plains2");
			var waterAsset = self.queue.getResult("water");
			var tile = new createjs.Bitmap(tileAsset);
			var x, y;
			var rows = 10;
			var cols = 20;
			
			var hexDetails = {"width": tile.image.width, "height": tile.image.height, "c": 19};
			
			self.hexuMap = new hexU.HexMap(cols, rows, hexDetails, "odd-r");
			var tiles = self.hexuMap.getTiles();
			
			self.tileMat = [];
			for (var i = 0; i < self.hexuMap.rows; i++) {
				self.tileMat[i] = [];
				for (var j = 0; j < self.hexuMap.columns; j++) {
					var pos = tiles[i][j];
					
					// naive random terrain generation
					var water = false;
					var newTile;
					if (Math.random() >= 0.9) {
						var newTile = new createjs.Bitmap(waterAsset);
						water = true;
					} else {
						var newTile = new createjs.Bitmap(tileAsset);
					}

					newTile.x = pos.x;
					newTile.y = pos.y;
					
					var hillTile = new createjs.Bitmap(hillsAsset);
					hillTile.x = pos.x + 5;
					hillTile.y = pos.y - 10;
					console.log(hillTile.getBounds());

					var hillTile2 = new createjs.Bitmap(hillsAsset2);
					hillTile2.x = pos.x + 5;
					hillTile2.y = pos.y - 10;
					console.log(hillTile2.getBounds());

					var plainsTile = new createjs.Bitmap(plainsAsset);
					plainsTile.x = pos.x;
					plainsTile.y = pos.y;

					var plainsTile2 = new createjs.Bitmap(plainsAsset2);
					plainsTile2.x = pos.x;
					plainsTile2.y = pos.y;
					
					self.tileMat[i][j] = newTile;
					self.mapContainer.addChild(newTile);
					if (!water) {
						if (Math.random() > 0.7) {
							if (Math.random() >= 0.5) {
								self.mapContainer.addChild(hillTile);
							} else {
								self.mapContainer.addChild(hillTile2);
							}
						} else if (Math.random() > 0.6) {
							if (Math.random() >= 0.5) {
								self.mapContainer.addChild(plainsTile);
							} else {
								self.mapContainer.addChild(plainsTile2);
							}
						}
					}

					// label
					var label = new createjs.Text(j + "," + i, "bold 12px Arial", "#000000");
					label.x = pos.x + (tile.image.width - label.getBounds().width) / 2;
					label.y = pos.y + (tile.image.height - label.getBounds().height) / 2;
					//self.mapContainer.addChild(label);

				}
			}
			
			// XXX attempting to cache for better performance
			self.mapContainer.cache(0, 0, self.mapContainer.getBounds().width, self.mapContainer.getBounds().height);
			
			self.mapEventContainer.graphics.beginFill("#FF0000").drawRect(0, 0, self.mapContainer.getBounds().width, self.mapContainer.getBounds().height);
			self.mapEventContainer.alpha = 0.2;
			
			var clickHandle = function(e) {
				// "normalize" the coordinates by calculating the position relative to the container's current (x,y)
				var mapX = e.stageX - self.mapEventContainer.x;
				var mapY = e.stageY - self.mapEventContainer.y;

				var pos = self.hexuMap.coordToHex(mapX, mapY);
				
				if (self.gameState == GameStateEnum.SIMPLE) {
					if (self.hexuMap.isPosValid(pos.c, pos.r)) {
						self.mapContainer.removeChild(self.tileMat[pos.r][pos.c]);
						self.mapContainer.updateCache();
					}
				} else if (self.gameState == GameStateEnum.NEIGHBORS || self.gameState == GameStateEnum.DIAGONALS) {
					for (var i = 0; i < 6; i++) {
						var found = self.gameState == GameStateEnum.NEIGHBORS ? self.hexuMap.getNeighbor(pos.c, pos.r, i) : self.hexuMap.getDiagonal(pos.c, pos.r, i);
						if (self.hexuMap.isPosValid(found.c, found.r)) {
							console.log(found);
							self.mapContainer.removeChild(self.tileMat[found.r][found.c]);
							self.mapContainer.updateCache();
						}
					}
				}
				
				self.stage.update();
			};
			
			var tickHandle = function(event) {
				// Actions carried out each tick (aka frame)
				if (!event.paused) {
					 // Actions carried out when the Ticker is not paused.
					 self.stage.update();
					 console.log("tick");
				}
			}
			var pressupHandle = function(event) {
					console.log('pressup #' + event.pointerID + ' at ' + event.stageX + ', ' + event.stageY);
					createjs.Ticker.removeEventListener("tick", tickHandle);
					self.mapEventContainer.removeEventListener("pressup", pressupHandle);
					self.mapEventContainer.addEventListener("click", clickHandle);
					self.diffX = -1;
					self.diffY = -1;
				// make sure we render any last "extra" frame not caught by the ticker
				self.stage.update();
			}
			self.mapEventContainer.addEventListener("click", clickHandle);
			
			self.diffX = -1;
			self.diffY = -1;
			
			self.mapEventContainer.addEventListener("pressmove", function(event) {
				// console.log('pressmove #' + event.pointerID + ' at ' + event.stageX + ', ' + event.stageY);
				
				if (self.diffX == -1 && self.diffY == -1) {
					self.diffX = event.stageX - self.mapEventContainer.x;
					self.diffY = event.stageY - self.mapEventContainer.y;
					
					createjs.Ticker.addEventListener("tick", tickHandle);
					self.mapEventContainer.addEventListener("pressup", pressupHandle);
					self.mapEventContainer.removeEventListener("click", clickHandle);
				} else {
					self.mapContainer.x = event.stageX - self.diffX;
					self.mapContainer.y = event.stageY - self.diffY;
					self.mapEventContainer.x = event.stageX - self.diffX;
					self.mapEventContainer.y = event.stageY - self.diffY;
				}
				// self.hexuMap[pos.c][pos.r].alpha -= 0.01;
			});
			
			self.stage.update();
		}
		
		function Hud(game) {
			var self = this;
			this.toggle;
			this.label;
			
			this.init = function() {
				var button = new createjs.Shape();
				button.graphics.beginFill("DeepSkyBlue").drawRect(0, 0, 100, 30);
				button.setBounds(0, 0, 100, 30);
				
				self.toggle = new createjs.Container();
				
				self.label = new createjs.Text(game.gameState, "10pt Arial", "#FFFFFF");
				self.label.textAlign = "center";
				self.label.textBaseline = "middle";
				self.label.x = button.x + button.getBounds().width  / 2;
				self.label.y = button.y + button.getBounds().height / 2;
				
				self.toggle.addChild(button);
				self.toggle.addChild(self.label);
				
				self.toggle.addEventListener("click", function () {
					game.changeState();
					self.label.text = game.gameState;
					game.stage.update();
				});
				
				game.stage.addChild(self.toggle);
			}
		}

		function MainMenu(game) {

			var self = this;
			this.view = new createjs.Container();

			var buttonGraph = new createjs.Graphics();
			buttonGraph.beginFill("DeepSkyBlue").drawRect(0, 0, 200, 50);

			this.init = function() {
				var startButton = createButton("Start Game");
				this.view.addChild(startButton);

				this.view.x = (game.stage.canvas.width - this.view.getBounds().width) / 2;
				this.view.y = (game.stage.canvas.height - this.view.getBounds().height) / 2;

				this.view.addEventListener("click", function () { 
					self.hide(); 
					game.startGame(); 
				});
			}

			this.show = function() {
				game.stage.addChild(this.view);
				game.stage.update();
			}

			this.hide = function() {
				game.stage.removeChild(this.view);
				game.stage.update();
			}

			function createButton(text) {
				var button = new createjs.Container();

				var shape = new createjs.Shape(buttonGraph);
				// this should be the same bounds as the graphics
				shape.setBounds(0, 0, 200, 50);

				var label = new createjs.Text(text, "bold 20px Arial", "#FFFFFF");
				label.textAlign = "center";
				label.textBaseline = "middle";
				label.x = shape.getBounds().width  / 2;
				label.y = shape.getBounds().height / 2;

				button.addChild(shape,label);

				return button;
			}

			this.init();
		}
		
	}