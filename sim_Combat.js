function CombatSim(stage) {
	var self = this;
	this.engagement;
	
	this.init = function() {
		att = [];
		def = [];
		env = { terrain : _wt.TerrainEnum.PLAINS };
		
		// attacking
		att.push(
			{
				eqp: 2,
				cmd: 3,
				exp: 2,
				str: 10,
				coh: 10,
				sup: 5,
				type: _wt.UnitTypeEnum.BAT,
				player: _wt.PlayerEnum.P1
			}
		);
		
		// defending
		def.push(
			{
				eqp: 2,
				cmd: 3,
				exp: 2,
				str: 10,
				coh: 10,
				sup: 5,
				type: _wt.UnitTypeEnum.BAT,
				player: _wt.PlayerEnum.P2
			}
		);
		
		self.engagement = new Engagement(att, def, env);
	}
	
	this.start = function() {
		self.engagement.doTurn();
	}
}

function Engagement(attack, defence, env) {
	var self = this;
	this.att = createSide(attack);
	this.def = createSide(defence);
	this.env = env;
	
	this.doTurn = function() {
		// TODO
	}
	
	this.sumDamage = function(side) {
		// TODO
	}
	
	this.addUnit = function(unit) {
		addToSide( (unit.player === att.player ? self.att : self.def), unit );
	}
	
	function createSide(units) {
		var side = {};
		side.units = {};
		
		for (var i = 0; i < units.length; i++) {
			var unit = units[i];
			if (!side.player) {
				side.player = unit.player;
			}
			addToSide(side, unit);
		}
		return side;
	}
	
	function addToSide(side, unit) {
		if (!side.units[unit.type]) {
			side.units[unit.type] = [];
		}
		side.units[unit.type].push(unit);
	}
	
	console.log(this.att);
	console.log(this.def);
}