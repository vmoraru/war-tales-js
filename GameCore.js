this._wt = this._wt || {};

(function() {

	// Constants
	this._wt.TerrainEnum = { PLAINS: "plains", HILLS: "hills", URBAN: "urban", MOUNTAINS: "mountains", WATER: "water"};
	this._wt.UnitTypeEnum = { BAT: "batallion", ART: "artillery", AIR: "air"};
	this._wt.PlayerEnum = { P1: "player1", P2: "player2" };
	this._wt.CoreEvents = {
		PRELOAD_COMPLETE: "preloadComplete" 
	};
	this._wt.MapEvents = {
		MAP_CLICK: "map_click",
		MAP_PRESSUP: "map_pressup",
		MAP_PRESSMOVE: "map_pressmove",
		HEX_CLICK: "hex_click",
	};

	// Event dispatching
	this._wt.evtDispatcher = $({});

	// Global Game data
	this._wt.globals = {}; // -- gets loaded when game starts from: [game_settings.json]

	this._wt.hexDetails = function() {
		return _wt.globals.game_settings.map.hexDetails;
	}

	this._wt.mapSettings = function() {
		return _wt.globals.game_settings.map;
	}

}());