this._wt = this._wt || {};

(function() {

	this._wt.Game = function(stage, mapSettings, config) {

		_wt.stage = stage;
		_wt.preloader = new _wt.GamePreloader();
		$(_wt.evtDispatcher).on(_wt.CoreEvents.PRELOAD_COMPLETE, function(e) {
			console.log("[Event] " + e.type);
			_wt.gameBoard = new _wt.GameBoard(mapSettings);

			// start game loop
		});
	}

}());