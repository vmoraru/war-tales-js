this._wt = this._wt || {};

(function() {

	/**
	* GameBoard
	**/
	this._wt.GameBoard = function(settings) {

		var self = this;
		var hexuMap = new hexU.HexMap(settings.cols, settings.rows, _wt.hexDetails(), "odd-r");
		var tiles = hexuMap.getTiles();
		var boardView = new _wt.BoardView(mergeTiles(tiles, settings.tiles));
		var moveCtl = new _wt.BoardMoveController(boardView);
		boardView.enableTriggers();

		this.getTiles = function() {
			return tiles;
		}

		function mergeTiles(to, from) {
			if (!from) {
				return to;
			}

			for (var i in from) {
				for (var j in from) {
					for (var attrName in from) {
						to[i][j][attrName] = from[i][j][attrName];
					}
				}
			}

			return to;
		}

	}

	/**
	* BoardView
	**/
	this._wt.BoardView = function(tileData) {

		var self = this;
		var hexDetails = _wt.hexDetails();
		var hOutline = createHexOutline(hexDetails);

		var baseMap = new createjs.Container();
		var features = new createjs.Container();
		var outlines = new createjs.Container();
		var map = new createjs.Container();

		for (var i in tileData) {
			for (var j in tileData[i]) {
				var tile = tileData[i][j];

				// base hex graphics
				var base = createBaseTile(tile);
				baseMap.addChild(base);
				
				// hex features
				var feature = createTileFeatures(tile);
				features.addChild(feature);

				// outline
				var outl = hOutline.clone();
				outlines.addChild(outl);
				outl.x = tile.x;
				outl.y = tile.y;
			}
		}

		// create map event container
		var buffer = _wt.mapSettings().event_container_buffer;
		var bounds = [0 - buffer, 0 - buffer, baseMap.getBounds().width + 2*buffer, baseMap.getBounds().height + 2*buffer];
		var eventContainer = createEventContainer(bounds);

		map.addChild(eventContainer);
		map.addChild(baseMap);
		map.addChild(features);
		map.addChild(outlines);

		// TODO add positioning
		map.x = 200;
		map.y = 200;
		
		// cache map object
		map.cache.apply(map, bounds);
		_wt.stage.addChild(map);
	
		this.enableTriggers = function() {
			eventContainer.addEventListener("click", triggerMapEvent);
			eventContainer.addEventListener("pressup", triggerMapEvent);
			eventContainer.addEventListener("pressmove", triggerMapEvent);
		}

		this.disableTriggers = function() {
			eventContainer.removeEventListener("click", triggerMapEvent);
			eventContainer.removeEventListener("pressup", triggerMapEvent);
			eventContainer.removeEventListener("pressmove", triggerMapEvent);
		}

		this.getPos = function() {
			return {x: map.x, y: map.y};
		}

		this.setPos = function(pos) {
			map.x = pos.x;
			map.y = pos.y;
		}

		function triggerMapEvent(e) {
			// console.log(e);
			_wt.evtDispatcher.triggerHandler("map_" + e.type, e);
		}

		function createBaseTile(tile) {
			var baseTile = new createjs.Bitmap(_wt.preloader.getBaseTexture());
			baseTile.x = tile.x;
			baseTile.y = tile.y;

			return baseTile;
		}

		function createTileFeatures(tile) {
			var tileFeat = new createjs.Bitmap();
			tileFeat.x = tile.x;
			tileFeat.y = tile.y;

			return tileFeat;	
		}

		function createEventContainer(bounds) {
			var color = _wt.mapSettings().event_container_color;
			var alpha = _wt.mapSettings().event_container_alpha;
			var eventContainer = new createjs.Shape();

			eventContainer.graphics.beginFill(color).drawRect(bounds[0], bounds[1], bounds[2], bounds[3]);
			eventContainer.setBounds(bounds[0], bounds[1], bounds[2], bounds[3]);
			eventContainer.alpha = alpha;

			return eventContainer;
		}

		function createHexOutline(hexDetails) {
			var hex = new createjs.Shape();
			var g = hex.graphics;
			g.setStrokeStyle(_wt.mapSettings().hex_outline_thickness);
			g.beginStroke(_wt.mapSettings().hex_outline_color);
			g.moveTo(0, hexDetails.c);
			g.lineTo(0, hexDetails.height - hexDetails.c);
			g.lineTo(hexDetails.width / 2, hexDetails.height);
			g.lineTo(hexDetails.width, hexDetails.height - hexDetails.c);
			g.lineTo(hexDetails.width, hexDetails.c);
			g.lineTo(hexDetails.width / 2, 0);
			g.lineTo(0, hexDetails.c);
			
			return hex;
		}
		
	}

	this._wt.BoardMoveController = function(boardView) {
		var enabled = true;
		var scrolling = false;
		var diffX = -1;
		var diffY = -1;

		_wt.evtDispatcher.on(_wt.MapEvents.MAP_CLICK, clickHandle);
		_wt.evtDispatcher.on(_wt.MapEvents.MAP_PRESSMOVE, pressmoveHandle);

		function clickHandle(e, cause) {
			console.log(e.type);
			console.log(e);
			console.log(cause);
		}

		function pressupHandle(e, cause) {
			console.log(e.type);
			scrolling = false;
			_wt.evtDispatcher.off(_wt.MapEvents.MAP_PRESSUP, pressupHandle);
			_wt.evtDispatcher.on(_wt.MapEvents.MAP_CLICK, clickHandle);
		}

		function pressmoveHandle(e, cause) {
			console.log(e.type);
			if (!scrolling) {
				// first pressmove Event
				scrolling = true;
				var pos = boardView.getPos();
				diffX = cause.stageX - pos.x;
				diffY = cause.stageY - pos.y;
					
				_wt.evtDispatcher.off(_wt.MapEvents.MAP_CLICK, clickHandle);
				_wt.evtDispatcher.on(_wt.MapEvents.MAP_PRESSUP, pressupHandle);
			} else {
				var _x = cause.stageX - diffX;
				var _y = cause.stageY - diffY;
				boardView.setPos({ x: _x, y: _y});
			}
			
		}

	}

}());